# OpenML dataset: Another-Dataset-on-used-Fiat-500-(1538-rows)

https://www.openml.org/d/43828

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset has been created from a query done on an website specialized in used cars and contains 1538 rows
Description of colums:
model: Fiat 500 comes in several 'flavours' :'pop', 'lounge', 'sport'
engine_power: number of Kw of the engine
ageindays: age of the car in number of days (from the time the dataset has been created)
km: kilometers of the car
previous_owners: number of previous owners
lat: latitude of the seller (the price of cars in Italy varies from North to South of the country)
lon: longitude of the seller (the price of cars in Italy varies from North to South of the country)
price: selling price (the target)
I collected this dataset to train myself and test regression algorithms. Hope this can help people to train as well.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43828) of an [OpenML dataset](https://www.openml.org/d/43828). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43828/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43828/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43828/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

